﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EBook.ViewModels
{
	public class BookViewModel : ViewModel
	{
		[Required, MaxLength(80)]
		public string Title { get; set; }
		[Required, MaxLength(120)]
		public string Author { get; set; }
		[Required, MaxLength(120)]
		public string Keywords { get; set; }
		[Required]
		public DateTime PublicationYear { get; set; }
		public string FileName { get; set; }
		[NotMapped]
		public IFormFile BookFile { get; set; }
		[Required, MaxLength(100)]
		public string MIME { get; set; }
		public CategoryViewModel Category { get; set; }
		public LanguageViewModel Language { get; set; }
		
	}
}
